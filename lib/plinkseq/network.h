#ifndef __PSEQ_NET_H__
#define __PSEQ_NET_H__

#include <string>
#include <map>
#include <set>
#include <vector>
#include <ostream>

#include "sqlwrap.h"
#include "regions.h"
#include "matrix.h"

#include "plinkseq/netdb.h"
#include "plinkseq/net-connected-components.h"

/* Net(work) is based on Nodes and Edges.
 * Nodes point to a Gene (should make permutation easier, just shuffling pointers).
 * Edges relate to Nodes.
 */

namespace NetFunc {
	struct Group;

	class Gene {
	public:
		Gene(const std::string& label)
		: _label(label) {
		}

		Gene(const Gene& g)
		: _label(g._label) {
		}

		const std::string& getLabel() const { return _label; }

	private:
		const std::string _label;
	};

	class Group {
	public:
		Group() : _label("group") {}

		Group(const std::string& label)
		: _label(label) {
		}

		std::string getLabel() const { return _label; }

		void addMember(const Gene* g, double wgt = 1) {
			_members[g] = wgt;
		}

		unsigned int size() const {
			return _members.size();
		}

		bool empty() const {
			return _members.empty();
		}

		void clear() {
			_members.clear();
		}

		bool isMember(const Gene* g) const {
			MemberWeightMap::const_iterator findG = _members.find(g);
			return (findG != _members.end());
		}

		double getWeight(const Gene* g) const {
			double wgt = 0;

			MemberWeightMap::const_iterator findG = _members.find(g);
			if (findG != _members.end())
				wgt = findG->second;

			return wgt;
		}

		typedef std::map<const Gene*, double> MemberWeightMap;
		MemberWeightMap::const_iterator memberWeightBegin() const { return _members.begin(); }
		MemberWeightMap::const_iterator memberWeightEnd()   const { return _members.end(); }

	private:
		std::string _label;
		MemberWeightMap _members;
	};

	class Edge;

	class Node {
	public:
		Node(Gene* g, int nodeInd) : _gene(g), _nodeInd(nodeInd) {}

		void setGene(Gene* g) { _gene = g; }
		const Gene* getGene() const { return _gene; }
		Gene* getGene() { return _gene; }

		int getNodeInd() const { return _nodeInd; }

		// primary representation of network (i.e. connection from this node to other nodes)
		typedef std::set<Edge> Neighbors;

		bool addEdgeTo(const Node* toNode, double w);
		bool hasEdgeTo(const Node* toNode) const;
		bool removeEdgeTo(const Node* toNode);

		inline unsigned int numNeighbors() const { return _neighbors.size(); }

		Neighbors::const_iterator neighbBegin() const { return _neighbors.begin(); }
		Neighbors::const_iterator neighbEnd() const { return _neighbors.end(); }

		void removeOutgoingEdges() { _neighbors.clear(); }

		bool operator<(const Node& rhs) const {
			return _nodeInd < rhs._nodeInd;
		}
		bool operator==(const Node& rhs) const {
			return _nodeInd == rhs._nodeInd;
		}
		bool operator!=(const Node& rhs) const {
			return !(*this == rhs);
		}

	private:
		Gene* _gene;
		int _nodeInd;

		Neighbors _neighbors;
	};

	class Edge {
	public:
		Edge(const Node* fromNode, const Node* toNode, double w = 1)
		: fromNode(fromNode), toNode(toNode), wgt(w) {
		}

		bool operator<(const Edge& rhs) const {
			if (*fromNode != *rhs.fromNode)
				return *fromNode < *rhs.fromNode;
			if (*toNode != *rhs.toNode)
				return *toNode < *rhs.toNode;
			return false;
		}

		const Node* getFromNode() const { return fromNode; }
		const Node* getToNode()   const { return toNode; }
		double getWeight() const { return wgt; }

	private:
		const Node* fromNode;
		const Node* toNode;
		double wgt; // weight from a to b
	};

	struct paths_t {
		int size() const {
			return paths.size();
		}
		std::vector<std::vector<Node*> > paths;
		std::vector<double> wgts;
	};

	// this is the actual in-memory representation of the network
	class Net {
		class DegreeStats;
		friend class DegreeStats;

	public:
		Net(NetDBase*, bool edge_weights, std::set<std::string>* excludes = NULL);

		Net(const Net& net);
		Net& operator=(const Net& net);

		~Net();

		int addNode(Gene* g);

		bool addUndirectedEdge(int ind1, int ind2, double w);
		bool addDirectedEdge(int fromInd, int toInd, double w);

		bool removeUndirectedEdge(int ind1, int ind2);
		bool removeDirectedEdge(int fromInd, int toInd);
		void removeAllEdges();

		bool hasUndirectedEdge(int ind1, int ind2) const { return hasDirectedEdge(ind1, ind2) && hasDirectedEdge(ind2, ind1); }
		bool hasDirectedEdge(int fromInd, int toInd) const { return _nodes[fromInd]->hasEdgeTo(_nodes[toInd]); }

		// Returns true iff checkIndex is in-bounds and it is currently a Node in this Net
		bool hasNode(int checkIndex) const { return checkIndex < numNodes(); }
		inline unsigned int numNodes() const { return _nodes.size(); }
		inline unsigned int numNeighbors(int index) const { if (!hasNode(index)) return 0; return _nodes[index]->numNeighbors(); }

		std::vector<std::string> node_labels() const;

		// degree of individual gene
		int degree(const std::string& g) const;

		Data::Matrix<double> Normalized_Laplacian() const;
		Data::Matrix<double> Laplacian() const;
		Data::Matrix<double> Adjacency() const;
		Data::Matrix<double> Influence_Matrix(const double gamma = 1) const;

		void basic_n(int* n_nodes, int* n_edges, int* n_nodes_in_network) const;

		// return mean and (optionally) whole degree-distribution (in 'dd')
		double degree_distribution(double* x0, std::map<int, int>* dd = NULL);

		// seeds
		bool addSeed(const std::string& s, double wgt = 1);
		void clearSeeds();

		// optional second seed list
		bool addCseed(const std::string& s, double wgt = 1);
		void clearCseeds();

		void set_cseed_param(bool a, bool b) {
			if (a != _fix_cseed_node_perms) {
				_fix_cseed_node_perms = a;
				_degreeStats->clearDegrees();
			}

			_include_cseed_cseed_OR_seed_seed_connections_in_cross = b;
		}

		// use edge weights?
		void use_edge_weights(const bool b) {
			_use_edge_weights = b;
		}
		bool use_edge_weights() const {
			return _use_edge_weights;
		}

		/* set merge threshold for node degree-binning.
		 *
		 * Value --> meaning:
		 *
		 *  0     -->  complete merging (all degrees to one bin, i.e. no matching on degree)
		 *  1     -->  no binning
		 *  N > 2 -->  ensure degree-bins have at least N nodes
		 */
		void degree_merge_threshold(const int t) {
			if (t != _degree_merge_threshold) {
				_degree_merge_threshold = t;
				_degreeStats->clearDegrees();
			}
		}
		int degree_merge_threshold() const {
			return _degree_merge_threshold;
		}

		// use edge-permutation for unique degrees
		void edge_permutation(const bool b) {
			_use_edge_permutation = b;
		}
		bool edge_permutation() const {
			return _use_edge_permutation;
		}

		// using seed permutation (then, e.g., Net doesn't need to calculate genic scores)
		void seed_permutation(const bool b) {
			_use_seed_permutation = b;
		}
		bool seed_permutation() const {
			return _use_seed_permutation;
		}

		bool has_cseed_list() const {
			return !_cseeds.empty();
		}

		// options in net-stats (i.e., to speed up, can drop the calculation of some of these)
		void calc_common_interactors(const bool b) {
			_calc_common_interactors = b;
		}
		bool calc_common_interactors() const {
			return _calc_common_interactors;
		}

		void calc_genic_scores(const bool b) {
			_calc_genic_scores = b;
		}
		bool calc_genic_scores() const {
			return _calc_genic_scores;
		}

		void calc_general_connectivity(const bool b) {
			_calc_general_connectivity = b;
		}
		bool calc_general_connectivity() const {
			return _calc_general_connectivity;
		}

		void calc_indirect_connectivity(const bool b) {
			_calc_indirect_connectivity = b;
		}
		bool calc_indirect_connectivity() const {
			return _calc_indirect_connectivity;
		}

		typedef std::map<std::string, std::string> KeyValMap;
		typedef std::map<std::string, KeyValMap> KeyKeyValMap;

		typedef std::map<std::string, double> NameToVal;

		// main netstats driver:
		NameToVal netstats_full(KeyKeyValMap* rpnt = NULL) const;

		static void update_statistics(const NameToVal&, const NameToVal&, NameToVal*, NameToVal*);

		// (direct) connections
		std::map<std::string, int> connections(const std::string& gene, const int depth = 1) const;

		// seed/bin stats
		typedef std::map<std::string, std::vector<double> > BinToStats;
		typedef std::map<std::string, std::string> BinToLabel;
		BinToStats seed_bin_stats(BinToLabel* labels);

		//
		// Graph algorithms
		//

		// shortest paths interface
		paths_t Dijkstra(Node* a) const;

		// connected components
		typedef DisjointSet<const Node*>::EquivSet EquivNodes;
		typedef DisjointSet<const Node*>::EquivSetList EquivNodesList;
		const EquivNodesList* calc_connected_components();

		typedef std::vector<std::set<std::string> > ConnComp;
		ConnComp get_connected_components_of_size(int s, int t = -1);

		int n_connected_components() {
			if (_components == NULL)
				calc_connected_components();
			return _components->size();
		}

		void clear_connected_components() {
			if (_components != NULL) {
				delete _components;
				_components = NULL;
			}
		}

		int n_seeds() const {
			return _seeds.size();
		}
		bool isSeed(const Node* node) const {
			return isSeed(node->getGene());
		}
		bool isSeed(const Gene* gene) const {
			return _seeds.isMember(gene);
		}

		int n_cseeds() const {
			return _cseeds.size();
		}
		bool isCseed(const Node* node) const {
			return isCseed(node->getGene());
		}
		bool isCseed(const Gene* gene) const {
			return _cseeds.isMember(gene);
		}

		void permuteNodeGeneRelationships();

		// make a subnetwork, including only existing edges with weight >= delta:
		void create_subnetwork(double delta);

		// matrix -> network
		void create_network_from_matrix(const Data::Matrix<double>& m,
				const double thr, bool greater_than = true);

		// remove edges in existing network if they are below/above threshold
		void remove_edges_based_on_matrix_vals(const Data::Matrix<double>& m,
				const double thr, bool remove_less_than = true);

		void remove_edges_from_matrix_seed_score_enhanced(const Data::Matrix<double>& m, const double thr);

		typedef std::map<const Gene*, int> GeneToInt;
		typedef std::set<const Gene*> ConstGeneSet;
		typedef std::map<const Gene*, ConstGeneSet> GeneToGenes;
		typedef std::set<Edge> ConstEdgeSet;

	private:
		// calc/store degree of each node
		DegreeStats* _degreeStats;

		// permute network
		bool _use_edge_permutation;
		bool _use_seed_permutation;

		// use edge weights from NETDB?
		bool _use_edge_weights;

		// two-seed list mode?
		bool _fix_cseed_node_perms;
		bool _include_cseed_cseed_OR_seed_seed_connections_in_cross;

		// see degree_merge_threshold() above for definition
		int _degree_merge_threshold;

		// options for net-statistics
		bool _calc_common_interactors;
		bool _calc_genic_scores;
		bool _calc_general_connectivity;
		bool _calc_indirect_connectivity;

		// 'human' label to internal 'Gene' representation
		typedef std::map<std::string, Gene*> LabelToGene;
		LabelToGene _label2gene;

		typedef std::list<Node*> NodeList;
		typedef std::list<const Node*> ConstNodeList;

		typedef std::vector<Node*> NodeVec;
		typedef std::vector<const Node*> ConstNodeVec;

		NodeVec _nodes;

		// primary store used to iterate over all genes, and do look-ups
		typedef std::map<Gene*, Node*> GeneToNode;
		GeneToNode _geneToNode;

		// connected _components
		EquivNodesList* _components;

		// seed genes
		Group _seeds;

		// second group seed list (comparator seeds, _cseeds)
		Group _cseeds;

		static const std::string DELTA_STATS_ARRAY[];
		static std::list<std::string> DELTA_STATS;

		/*
		 * FUNCTIONS
		 */
		void copyOtherNet(const Net& net);
		static Group copyGroup(const Group& group, const std::map<const Gene*, const Gene*>& copyToNewGene);

		void deleteDataMembers();

		typedef std::map<int, NodeVec> DegreeToNodes;
		typedef std::map<int, std::string> DegreeToLabel;

		class DegreeStats {
		public:
			DegreeStats(const Net* net) : _net(net),
			_degree2node(), _merged_degree2node(), _merged_degree_labels() {}

			// Assumes that ds and this point to _net objects with the same number of Nodes:
			DegreeStats& operator=(const DegreeStats& ds);

			void clearDegrees() {
				_degree2node.clear();
				_merged_degree2node.clear();
				_merged_degree_labels.clear();
			}

			inline const DegreeToNodes& degree2node() { if (_degree2node.empty()) {updateDegrees();} return _degree2node; }
			inline const DegreeToNodes& merged_degree2node() { if (_merged_degree2node.empty()) {updateDegrees();} return _merged_degree2node; }
			inline const DegreeToLabel& merged_degree_labels() { if (_merged_degree_labels.empty()) {updateDegrees();} return _merged_degree_labels; }

			// this rebuilds the network w/ the current binning
			// For example, will assign cseeds to their own bin size 1 (i.e. do not permute), if _fix_cseed_node_perms == true
			void updateDegrees();

		private:
			const Net* _net;

			DegreeToNodes _degree2node;
			DegreeToNodes _merged_degree2node;
			DegreeToLabel _merged_degree_labels;

			// helper functions:
			void populate_degree2node();
			void merge_degree2node(const int th);
		};

		Gene* find_gene(const std::string& label) const {
			return _label2gene.find(label) == _label2gene.end() ? NULL : _label2gene.find(label)->second;
		}

		// find all connecting nodes up to depth N
		std::map<const Node*, int> partners(const Node* node, const int depth = 1) const;

		static int numGenesNotInGroup(const ConstGeneSet& genes, const Group* excludeGroup = NULL);

		static void random_draw(std::vector<int>& a);

		struct SingleSeedGroupStats {
			int n_seeds;
			GeneToInt n_general_connections;
			GeneToInt n_direct_connections;
			GeneToInt n_indirect_connections;
			GeneToGenes interactors;
			GeneToInt interactor_count;
			ConstEdgeSet direct_edges;
			ConstGeneSet direct_nodes;
			ConstEdgeSet indirect_edges;
			ConstGeneSet indirect_nodes;
		};

		// helper function in netstats (does most of work for a given seed set):
		void netstats_singleSeedGroup(const Group* seeds,
				KeyKeyValMap* out,
				SingleSeedGroupStats* groupStats) const;

		void addSingleSeedGroupStats(NameToVal& stats, const SingleSeedGroupStats* groupStats, int groupIndex) const;

		// Basic network features

		// shortest path
		std::vector<Net::ConstNodeList> DijkstraGetShortestPaths(const Node* vertex,
				const std::vector<Net::ConstNodeVec>* previous) const;

		void DijkstraGetShortestPathsHelper(const Node* vertex, Net::ConstNodeList path,
				std::vector<Net::ConstNodeList>* paths,
				const std::vector<Net::ConstNodeVec>* previous) const;

		void DijkstraComputePaths(const Node* source,
				std::vector<double>& min_distance,
				std::vector<ConstNodeVec>& previous) const;
	};
}

#endif
