#include "plinkseq/network.h"
#include "plinkseq/helper.h"
#include "plinkseq/crandom.h"
#include "plinkseq/statistics.h"
#include "plinkseq/net-connected-components.h"

#include <queue>
#include <cmath>
#include <algorithm>
#include <list>

#include <limits>

/*
 * Node
 */
bool NetFunc::Node::addEdgeTo(const Node* toNode, double w) {
	_neighbors.insert(Edge(this, toNode, w)); return true;
}

bool NetFunc::Node::hasEdgeTo(const Node* toNode) const {
	return _neighbors.find(Edge(this, toNode)) != _neighbors.end();
}

bool NetFunc::Node::removeEdgeTo(const Node* toNode) {
	Neighbors::iterator findEdgeIt = _neighbors.find(Edge(this, toNode));
	if (findEdgeIt == _neighbors.end())
		return false;

	_neighbors.erase(findEdgeIt);
	return true;
}

/*
 * Net
 */
NetFunc::Net::Net(NetDBase* netdb, bool edge_weights, std::set<std::string>* excludes)
: _use_edge_permutation(false), // do not use edge permutation
  _use_seed_permutation(false), // node permutation by default
  _use_edge_weights(edge_weights), // use edge-weights from NETDB
  _fix_cseed_node_perms(true),
  _include_cseed_cseed_OR_seed_seed_connections_in_cross(false),
  _degree_merge_threshold(10), // 'safe' degree merge threshold
  // net-statistics to calculate
  _calc_common_interactors(true),
  _calc_genic_scores(true),
  _calc_general_connectivity(true),
  _calc_indirect_connectivity(true),
  _label2gene(), _geneToNode(), _nodes(),
  _degreeStats(NULL),
  _components(NULL),
  _seeds("__SEEDS__"), // primary seed gene list
  _cseeds("__CSEEDS__") { // secondary, comparator list

	_degreeStats = new DegreeStats(this);

	//
	// Load from NETDB
	//
	std::vector<int> a, b;
	std::vector<double> w; // edge weights (optional)

	//  std::cout << "using edge w " << use_edge_weights << "\n";

	std::map<int, std::string> dbnodes = netdb->fetch_all(a, b, _use_edge_weights ? &w : NULL);
	//plog << "found " << dbnodes.size() << " nodes and " << a.size() << " edges\n" ;

	//
	// construct gene/nodes
	//
	std::map<int, int> id2node;
	std::set<std::string> check_for_unique_genes;

	// start processing node list
	for (std::map<int, std::string>::const_iterator ii = dbnodes.begin(); ii != dbnodes.end(); ++ii) {
		const int geneIndInNetDB = ii->first;
		const std::string& geneName = ii->second;

		if (check_for_unique_genes.find(geneName) != check_for_unique_genes.end())
			continue;

		// implement a background list check?
		if (excludes && excludes->find(geneName) != excludes->end())
			continue;

		check_for_unique_genes.insert(geneName);
		Gene* gene = new Gene(geneName);
		_label2gene[geneName] = gene;

		id2node[geneIndInNetDB] = addNode(gene);
	}

	// add edges, processing edge list.
	// if gene excluded, should return a null value from the temporary id2node, so OK.
	for (int e = 0; e < a.size(); e++) {
		std::map<int, int>::const_iterator findNode1It = id2node.find(a[e]);
		std::map<int, int>::const_iterator findNode2It = id2node.find(b[e]);

		if (findNode1It != id2node.end() && findNode2It != id2node.end()) {
			int nd1 = findNode1It->second;
			int nd2 = findNode2It->second;
			if (nd1 != nd2)
				addUndirectedEdge(nd1, nd2, _use_edge_weights ? w[e] : 1.0);

			// check that this hasn't already been added
			// if ( p1->neighbors.find(e1) != p1->neighors.end() )
			//   {
			//     std::cout << "nodes " << a[e] << " and " << b[e] << "\n";
			//     Helper::halt( "NETDB contains duplicate entries: note, assume a undirected graph. Please remake the NETDB" );
			//   }
		}
	}
}

void NetFunc::Net::copyOtherNet(const Net& net) {
	_degreeStats = new DegreeStats(this);

	_use_edge_permutation = net._use_edge_permutation;
	_use_seed_permutation = net._use_seed_permutation;

	_use_edge_weights = net._use_edge_weights;

	_fix_cseed_node_perms = net._fix_cseed_node_perms;
	_include_cseed_cseed_OR_seed_seed_connections_in_cross = net._include_cseed_cseed_OR_seed_seed_connections_in_cross;

	_degree_merge_threshold = net._degree_merge_threshold;

	_calc_common_interactors = net._calc_common_interactors;
	_calc_genic_scores = net._calc_genic_scores;
	_calc_general_connectivity = net._calc_general_connectivity;
	_calc_indirect_connectivity = net._calc_indirect_connectivity;

	// Copy the genes and nodes:
	std::map<const Gene*, const Gene*> copyToNewGene;
	for (NodeVec::const_iterator nodeIt = net._nodes.begin(); nodeIt != net._nodes.end(); ++nodeIt) {
		const Node* copyNode = *nodeIt;
		const Gene* copyGene = copyNode->getGene();

		Gene* g = new Gene(*copyGene);
		_label2gene[g->getLabel()] = g;

		addNode(g);

		copyToNewGene[copyGene] = g;
	}

	// Copy the edges:
	for (NodeVec::const_iterator nodeIt = net._nodes.begin(); nodeIt != net._nodes.end(); ++nodeIt) {
		const Node* copyNode = *nodeIt;
		for (Node::Neighbors::const_iterator neighbIt = copyNode->neighbBegin(); neighbIt != copyNode->neighbEnd(); ++neighbIt) {
			const Edge& copyEdge = *neighbIt;
			addDirectedEdge(copyEdge.getFromNode()->getNodeInd(), copyEdge.getToNode()->getNodeInd(), copyEdge.getWeight());
		}
	}

	_components = NULL;

	// Copy the group memberships:
	_seeds = copyGroup(net._seeds, copyToNewGene);
	_cseeds = copyGroup(net._cseeds, copyToNewGene);

	// Ensure that net's stats are up-to-date before copying from it:
	net._degreeStats->degree2node();

	// Update the degree statistics, copying from net (but updating to point this Net's nodes):
	*_degreeStats = *(net._degreeStats);
}

NetFunc::Net::DegreeStats& NetFunc::Net::DegreeStats::operator=(const NetFunc::Net::DegreeStats& ds) {
	if (&ds != this) {
		_degree2node = ds._degree2node;
		_merged_degree2node = ds._merged_degree2node;
		_merged_degree_labels = ds._merged_degree_labels;

		for (DegreeToNodes::iterator i = _degree2node.begin(); i != _degree2node.end(); ++i) {
			NodeVec& nodes = i->second;
			for (NodeVec::iterator j = nodes.begin(); j != nodes.end(); ++j) {
				const Node* copyNode = *j;
				*j = _net->_nodes[copyNode->getNodeInd()];
			}
		}

		for (DegreeToNodes::iterator i = _merged_degree2node.begin(); i != _merged_degree2node.end(); ++i) {
			NodeVec& nodes = i->second;
			for (NodeVec::iterator j = nodes.begin(); j != nodes.end(); ++j) {
				const Node* copyNode = *j;
				*j = _net->_nodes[copyNode->getNodeInd()];
			}
		}
	}

	return *this;
}

NetFunc::Group NetFunc::Net::copyGroup(const Group& group, const std::map<const Gene*, const Gene*>& copyToNewGene) {
	Group g(group.getLabel());

	for (Group::MemberWeightMap::const_iterator membIt = group.memberWeightBegin(); membIt != group.memberWeightEnd(); ++membIt) {
		const Gene* copyGene = membIt->first;
		const double weight = membIt->second;

		std::map<const Gene*, const Gene*>::const_iterator findGeneIt = copyToNewGene.find(copyGene);
		if (findGeneIt != copyToNewGene.end())
			g.addMember(findGeneIt->second, weight);
	}

	return g;
}

NetFunc::Net::Net(const Net& net) {
	copyOtherNet(net);
}

NetFunc::Net& NetFunc::Net::operator=(const Net& net) {
	if (&net != this) {
		deleteDataMembers();
		copyOtherNet(net);
	}
	return *this;
}

void NetFunc::Net::deleteDataMembers() {
	if (_degreeStats != NULL)
		delete _degreeStats;

	for (LabelToGene::const_iterator geneIt = _label2gene.begin(); geneIt != _label2gene.end(); ++geneIt) {
		if (geneIt->second)
			delete geneIt->second;
	}

	for (NodeVec::const_iterator jj = _nodes.begin(); jj != _nodes.end(); ++jj) {
		if (*jj)
			delete *jj;
	}

	if (_components != NULL)
		delete _components;
}

NetFunc::Net::~Net() {
	deleteDataMembers();
}

int NetFunc::Net::addNode(Gene* g) {
	int ind = static_cast<int>(_nodes.size());
	Node* node = new Node(g, ind);
	_nodes.push_back(node);

	_geneToNode[g] = node;

	_degreeStats->clearDegrees();

	return ind;
}

bool NetFunc::Net::addUndirectedEdge(int ind1, int ind2, double w) {
	bool add1 = addDirectedEdge(ind1, ind2, w);
	bool add2 = addDirectedEdge(ind2, ind1, w);

	return add1 && add2;
}

bool NetFunc::Net::addDirectedEdge(int fromInd, int toInd, double w) {
	_nodes[fromInd]->addEdgeTo(_nodes[toInd], w);
	_degreeStats->clearDegrees();
	return true;
}

bool NetFunc::Net::removeUndirectedEdge(int ind1, int ind2) {
	bool remove1 = removeDirectedEdge(ind1, ind2);
	bool remove2 = removeDirectedEdge(ind2, ind1);

	return remove1 && remove2;
}

bool NetFunc::Net::removeDirectedEdge(int fromInd, int toInd) {
	if (_nodes[fromInd]->removeEdgeTo(_nodes[toInd])) {
		_degreeStats->clearDegrees();
		return true;
	}

	return false;
}

void NetFunc::Net::removeAllEdges() {
	for (NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii)
		(*ii)->removeOutgoingEdges();

	_degreeStats->clearDegrees();
}

void NetFunc::Net::DegreeStats::updateDegrees() {
	clearDegrees();

	// Calculate node degrees:
	populate_degree2node();

	// Merge nodes based on degree:
	merge_degree2node(_net->_degree_merge_threshold);
}

void NetFunc::Net::DegreeStats::populate_degree2node() {
	_degree2node.clear();

	for (GeneToNode::const_iterator ii = _net->_geneToNode.begin(); ii != _net->_geneToNode.end(); ++ii) {
		Node* node = ii->second;
		int degree = node->numNeighbors();
		_degree2node[degree].push_back(node);
	}
}

/* Bin nodes based on the following schemes:
 *
 * _degree_merge_threshold --> meaning:
 *
 *  0     -->  complete merging (all degrees to one bin, i.e. no matching on degree)
 *  1     -->  no binning
 *  N > 2 -->  ensure degree-bins have at least N nodes
 */
void NetFunc::Net::DegreeStats::merge_degree2node(const int th) {
	// if fix_cseed_node_perms has been set to true, need to
	// take care of this (i.e. do not permute cseeds).
	// normally, this is called before the seeds/cseeds are
	// read in; in this case, this option will have no effect;
	// however, when we specify a cseed list, we will call this
	// again after reading the cseeds

	_merged_degree2node.clear();
	_merged_degree_labels.clear();

	// special case: if 1, means no exact degree matching, so
	if (th == 1 && ((!_net->has_cseed_list()) || !_net->_fix_cseed_node_perms)) {
		_merged_degree2node = _degree2node;

		for (DegreeToNodes::const_iterator ii = _degree2node.begin(); ii != _degree2node.end(); ++ii)
			_merged_degree_labels[ii->first] = "[" + Helper::int2str(ii->first) + "-" + Helper::int2str(ii->first) + "]";

		return;
	}

	// special case: if 0, means no degree-matching, put everybody in a single set
	if (th == 0 && ((!_net->has_cseed_list()) || !_net->_fix_cseed_node_perms)) {
		NodeVec t;
		for (DegreeToNodes::const_iterator ii = _degree2node.begin(); ii != _degree2node.end(); ++ii) {
			const NodeVec& nodes = ii->second;
			for (int i = 0; i < nodes.size(); ++i)
				t.push_back(nodes[i]);
		}
		_merged_degree2node[0] = t;
		_merged_degree_labels[0] = "[no degree-matching]";
		return;
	}

	// Ensure each bin contains at least *th* nodes of consecutive degree values:
	NodeVec curBinNodes;
	int curBinInd = 0;
	int curBinStartDeg = -1;
	int curBinEndDeg = -1;
	int cseedBinInd = -1; // use -ve numbers for fixing cseed genes, should be okay [check]

	for (DegreeToNodes::const_iterator ii = _degree2node.begin(); ii != _degree2node.end(); ++ii) {
		int degree = ii->first;
		curBinEndDeg = degree;
		if (curBinStartDeg == -1)
			curBinStartDeg = curBinEndDeg;

		const NodeVec& degreeNodes = ii->second;
		for (NodeVec::const_iterator nodeIt = degreeNodes.begin(); nodeIt != degreeNodes.end(); ++nodeIt) {
			Node* node = *nodeIt;
			if (_net->has_cseed_list() && _net->_fix_cseed_node_perms && _net->isCseed(node)) {
				// add to a unique bin:
				_merged_degree2node[cseedBinInd] = NodeVec(1, node);
				_merged_degree_labels[cseedBinInd] = "[fixed cseed]";
				--cseedBinInd;
			}
			else
				curBinNodes.push_back(node);
		}

		if (curBinNodes.size() >= th) {
			_merged_degree2node[curBinInd] = curBinNodes;
			_merged_degree_labels[curBinInd] = "[" + Helper::int2str(curBinStartDeg) + "-" + Helper::int2str(curBinEndDeg) + "]";
			curBinStartDeg = curBinEndDeg = -1;
			++curBinInd;
			curBinNodes.clear();
		}
	}

	// did the last set of Nodes get added?
	if (!curBinNodes.empty()) {
		if (curBinNodes.size() >= th / 2.0) // can make additional final bin if this contains more than 50% of the required bin size
			curBinStartDeg = curBinEndDeg;
		else // otherwise, revert to previous bin
			--curBinInd;

		if (curBinInd == -1)
			curBinInd = 0;

		for (NodeVec::const_iterator nodeIt = curBinNodes.begin(); nodeIt != curBinNodes.end(); ++nodeIt)
			_merged_degree2node[curBinInd].push_back(*nodeIt);

		_merged_degree_labels[curBinInd] = "[" + Helper::int2str(curBinStartDeg) + "-" + Helper::int2str(curBinEndDeg) + "]";
	}
}

void NetFunc::Net::basic_n(int* n_nodes, int* n_edges, int* n_nodes_in_network) const {
	*n_nodes = _nodes.size();
	int e = 0;
	int n1 = 0; // nodes w/ 1 or more edge

	for (NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii) {
		const Node* nd = *ii;
		e += nd->numNeighbors();
		if (nd->numNeighbors() > 0)
			++n1;
	}

	// For an undirected graph, e should be an even #
	*n_edges = int(e / 2.0);
	*n_nodes_in_network = n1;
}

double NetFunc::Net::degree_distribution(double* x0, std::map<int, int>* dd) {
	// calculate mean degree
	int numer = 0;
	int denom = 0;
	int denomx0 = 0; // do not include 0-degree nodes in the mean (returned in x0)

	if (dd)
		dd->clear();

	for (DegreeToNodes::const_iterator jj = _degreeStats->degree2node().begin(); jj != _degreeStats->degree2node().end(); ++jj) {
		int degree = jj->first;
		int numWithDegree = jj->second.size();

		numer += degree * numWithDegree;
		denom += numWithDegree;
		if (degree > 0)
			denomx0 += numWithDegree;

		if (dd)
			(*dd)[degree] = numWithDegree;
	}

	*x0 = denomx0 == 0 ? 0.0 : (double) numer / (double) denomx0;

	return denom == 0 ? 0.0 : (double) numer / (double) denom;
}

// permute network (in-degree, followed by edge permutation)
void NetFunc::Net::permuteNodeGeneRelationships() {
	// node permutation, but stratified by 'merged_degree2node' groupings
	// optional edge permutation, for otherwise un-permuted nodes

	// if ( use_edge_permutation )
	//   edge_permutation_originals.clear();

	for (DegreeToNodes::const_iterator jj = _degreeStats->merged_degree2node().begin(); jj != _degreeStats->merged_degree2node().end(); ++jj) {
		const NodeVec& d = jj->second;
		const int n = d.size();

		// no point in permuting these
		if (n == 1) {
			// requires edge permutation?
			// if ( use_edge_permutation )
			//   edge_permutation_originals[ d[0] ] = (*d[0]).conn;
			continue;
		}

		// shuffle
		std::vector<int> a(n, 0);
		random_draw(a);

		// Store the original genes list
		std::vector<Gene*> o(n);
		for (int i = 0; i < n; i++)
			o[i] = d[i]->getGene();

		// Swap the Node -> Gene, Gene -> Node mappings:
		for (int i = 0; i < n; i++) {
			// swap gene onto a different node
			d[i]->setGene(o[a[i]]);
			_geneToNode[d[i]->getGene()] = d[i];
		}
	}

	// determine whether we need to do edge permutation...

	// if ( false use_edge_permutation )
	//   {

	//     const int ne = edge_permutation_originals.size();
	//     int ce = 0;
	//     std::map<Node*,std::set<Connection> >::const_iterator ee = edge_permutation_originals.begin();
	//     while ( ee != edge_permutation_originals.end() )
	// 	{
	// 	  bool permuted = false;
	// 	  while ( ! permuted )
	// 	    {
	// 	      // choose a conncetion
	// 	      int r = CRandom::rand( ne );
	// 	      // no self-self
	// 	      if ( ne == ce ) continue;

	// 	    }

	// 	  ++ee;
	// 	}
	//   }
}

Data::Matrix<double> NetFunc::Net::Adjacency() const {
	const int n = _nodes.size();
	Data::Matrix<double> A(n, n, 0);

	for (NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii) {
		const Node* nd1 = *ii;
		for (Node::Neighbors::const_iterator jj = nd1->neighbBegin(); jj != nd1->neighbEnd(); ++jj) {
			const Node* nd2 = jj->getToNode();
			A[nd1->getNodeInd()][nd2->getNodeInd()] = 1;
		}
	}

	return A;
}

Data::Matrix<double> NetFunc::Net::Laplacian() const {
	// degree matrix - adjacency matrix
	// http://en.wikipedia.org/wiki/Laplacian_matrix

	const int n = _nodes.size();
	Data::Matrix<double> L(n, n, 0);

	for (NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii) {
		const Node* nd1 = *ii;
		for (Node::Neighbors::const_iterator jj = nd1->neighbBegin(); jj != nd1->neighbEnd(); ++jj) {
			const Node* nd2 = jj->getToNode();
			L[nd1->getNodeInd()][nd2->getNodeInd()] = -1;
		}

		// degree on diagonal
		L[nd1->getNodeInd()][nd1->getNodeInd()] = nd1->numNeighbors();
	}

	return L;
}

Data::Matrix<double> NetFunc::Net::Normalized_Laplacian() const {
	// http://www.math.ucsd.edu/~fan/research/cb/ch1.pdf

	const int n = _nodes.size();
	Data::Matrix<double> L(n, n, 0);

	for (NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii) {
		const Node* nd1 = *ii;
		int degree = nd1->numNeighbors();

		for (Node::Neighbors::const_iterator jj = nd1->neighbBegin(); jj != nd1->neighbEnd(); ++jj) {
			const Node* nd2 = jj->getToNode();
			L[nd1->getNodeInd()][jj->getToNode()->getNodeInd()] = -1.0 / sqrt(degree * jj->getToNode()->numNeighbors());
		}

		// degree on diagonal
		L[nd1->getNodeInd()][nd1->getNodeInd()] = degree ? 1 : 0;
	}

	return L;
}

bool NetFunc::Net::addSeed(const std::string& s, double wgt) {
	Gene* gene = find_gene(s);
	if (gene == NULL)
		return false;
	_seeds.addMember(gene, wgt);
	return true;
}

void NetFunc::Net::clearSeeds() {
	_seeds.clear();
}

// ---- Comparator cseeds ------
bool NetFunc::Net::addCseed(const std::string& s, double wgt) {
	Gene* gene = find_gene(s);
	if (gene == NULL)
		return false;
	_cseeds.addMember(gene, wgt);

	// Since may want (or not want) to permute cSeeds, changing them require a rebinning of nodes to permute:
	_degreeStats->clearDegrees();
	return true;
}

void NetFunc::Net::clearCseeds() {
	_cseeds.clear();

	// Since may want (or not want) to permute cSeeds, changing them require a rebinning of nodes to permute:
	_degreeStats->clearDegrees();
}

// ---- Partner lookup for a gene ------------
std::map<std::string, int> NetFunc::Net::connections(const std::string& label, const int depth) const {
	std::map<std::string, int> s;

	Gene* gene = find_gene(label);
	if (gene == NULL)
		return s;

	std::cout << "found a non-null GENE\n";
	s[gene->getLabel()] = 0;

	Node* keynode = _geneToNode.find(gene)->second;

	std::map<const Node*, int> p = partners(keynode, depth);

	for (std::map<const Node*, int>::const_iterator ii = p.begin(); ii != p.end(); ++ii)
		s[ii->first->getGene()->getLabel()] = ii->second;

	return s;
}

std::map<const NetFunc::Node*, int> NetFunc::Net::partners(const Node* root, const int depth) const {
	std::map<const Node*, int> p;
	std::queue<const Node*> q;

	q.push(root);
	p[root] = 0;

	while (true) {
		if (q.size() == 0)
			break;
		const Node* u = q.front();
		q.pop();
		if (p[u] == depth)
			continue;

		for (Node::Neighbors::const_iterator ii = u->neighbBegin(); ii != u->neighbEnd(); ++ii) {
			const Node* v = ii->getToNode();
			if (p.find(v) == p.end()) {
				q.push(v);
				p[v] = p[u] + 1;
			}
		}
	}

	return p;
}

int NetFunc::Net::degree(const std::string& label) const {
	Gene* gene = find_gene(label);
	if (gene == NULL)
		return 0;

	const Node* node = _geneToNode.find(gene)->second;
	return node->numNeighbors();
}

//
// return network statistics
//
void NetFunc::Net::netstats_singleSeedGroup
		(const Group* seeds, // input
		KeyKeyValMap* out, // verbose output
		SingleSeedGroupStats* groupStats) const {

	groupStats->n_seeds = seeds->size();

	for (Group::MemberWeightMap::const_iterator seedIt = seeds->memberWeightBegin();
			seedIt != seeds->memberWeightEnd(); ++seedIt) {
		const Gene* seedGene = seedIt->first;

		// Ensure that seedGene is registered for these stats, even if none (ensures they will all be output):
		(groupStats->n_direct_connections)[seedGene] = 0;
		(groupStats->n_indirect_connections)[seedGene] = 0;

		// get actual node that corresponds to this gene
		const Node* seedNode = _geneToNode.find(const_cast<Gene*>(seedGene))->second;

		// general network seed connectivity
		(groupStats->n_general_connections)[seedGene] = seedNode->numNeighbors();

		for (Node::Neighbors::const_iterator neighbIt = seedNode->neighbBegin(); neighbIt != seedNode->neighbEnd(); ++neighbIt) {
			const Node* neighb = neighbIt->getToNode();

			// direct connectivity
			if (isSeed(neighb)) {
				++(groupStats->n_direct_connections)[seedGene];
				groupStats->direct_edges.insert(Edge(seedNode, neighb));
			}

			// Find all indirect interactors
			if (_calc_indirect_connectivity) {
				for (Node::Neighbors::const_iterator neighbNeighbIt = neighb->neighbBegin(); neighbNeighbIt != neighb->neighbEnd(); ++neighbNeighbIt) {
					const Node* neighbNeighb = neighbNeighbIt->getToNode();
					if (neighbNeighb == seedNode)
						continue;

					if (isSeed(neighbNeighb)) { // found an indirect seed-seed interaction (via neighb)
						groupStats->indirect_edges.insert(Edge(seedNode, neighbNeighb));

						// Tallies total # of indirect connections == number of paths such that: seedGene <---> CI <---> otherSeed [summed over all other seeds]
						(groupStats->n_indirect_connections)[seedGene]++;
					}
				}
			}

			// interactors is a map of: gene -> set of seed genes it interacts with
			if (_calc_common_interactors)
				(groupStats->interactors)[neighb->getGene()].insert(seedGene);
		}
	}

	// gather list of all genes in the direct network (direct node list)
	for (std::set<Edge>::const_iterator ed = groupStats->direct_edges.begin(); ed != groupStats->direct_edges.end(); ++ed) {
		const Gene* fromGene = ed->getFromNode()->getGene();
		const Gene* toGene = ed->getToNode()->getGene();

		if (fromGene->getLabel() < toGene->getLabel()) {
			// TODO ??? Ask Shaun ???
			// output individual gene scores, but not if using
			// seed-permutation, i.e. it does not make sense in that context
			if (out) {
				(*out)["gene_direct_node"][fromGene->getLabel()] = ".";
				(*out)["gene_direct_node"][toGene->getLabel()] = ".";
				(*out)["gene_direct_edge"][fromGene->getLabel() + " <--> " + toGene->getLabel()] = ".";
			}

			groupStats->direct_nodes.insert(fromGene);
			groupStats->direct_nodes.insert(toGene);
		}
	}

	// indirect nodes
	if (_calc_indirect_connectivity) {
		for (std::set<Edge>::const_iterator ei = groupStats->indirect_edges.begin(); ei != groupStats->indirect_edges.end(); ++ei) {
			const Gene* fromGene = ei->getFromNode()->getGene();
			const Gene* toGene = ei->getToNode()->getGene();

			if (fromGene->getLabel() < toGene->getLabel()) {
				if (out) {
					(*out)["gene_indirect_node"][fromGene->getLabel()] = ".";
					(*out)["gene_indirect_node"][toGene->getLabel()] = ".";
					(*out)["gene_indirect_edge"][fromGene->getLabel() + " <--> " + toGene->getLabel()] = ".";
				}

				groupStats->indirect_nodes.insert(fromGene);
				groupStats->indirect_nodes.insert(toGene);
			}
		}
	}

	// Stats on the common interactors (CI) themselves:
	// RECALL THAT: interactors = a map of: gene -> set of seed genes it interacts with
	for (GeneToGenes::const_iterator ciIt = groupStats->interactors.begin(); ciIt != groupStats->interactors.end(); ++ciIt) {
		const Gene* CI = ciIt->first;
		const ConstGeneSet& seedInteractors = ciIt->second;

		const int sz = seedInteractors.size();
		if (sz >= 2) { // CI connects 2 or more seeds
			(groupStats->interactor_count)[CI] = sz;

			// output gene list for original data
			if (out) {
				std::stringstream ss;
				ss << sz << "\t";

				for (ConstGeneSet::const_iterator gg = seedInteractors.begin(); gg != seedInteractors.end(); ++gg) {
					if (gg != seedInteractors.begin())
						ss << " ";
					ss << (*gg)->getLabel();
				}
				(*out)["common-interactor"][CI->getLabel()] = ss.str();
			}
		}
	}
}

void NetFunc::Net::addSingleSeedGroupStats(NameToVal& stats, const SingleSeedGroupStats* groupStats, int groupIndex) const {
	stringstream groupPrefixStream;
	groupPrefixStream << groupIndex << "_";
	string groupPrefix = groupPrefixStream.str();

	stats[groupPrefix + "n_seeds"] = groupStats->n_seeds;

	// --- General connectivity ---
	if (_calc_general_connectivity) {
		double tot_n_general_connections = 0;

		for (GeneToInt::const_iterator ii0 = groupStats->n_general_connections.begin(); ii0 != groupStats->n_general_connections.end(); ++ii0) {
			const Gene* seedGene = ii0->first;
			const int seedGeneGeneralConnections = ii0->second;

			tot_n_general_connections += seedGeneGeneralConnections;
			if (!_use_seed_permutation && _calc_genic_scores)
				stats["gene_" + groupPrefix + "general_score|" + seedGene->getLabel()] = seedGeneGeneralConnections;
		}
		stats[groupPrefix + "all_connectivity"] =
				groupStats->n_general_connections.size() == 0 ? 0 : tot_n_general_connections / (double) groupStats->n_general_connections.size();
	}

	// --- Direct network connectivity ---
	double tot_n_direct_connections = 0;

	for (GeneToInt::const_iterator ii1 = groupStats->n_direct_connections.begin(); ii1 != groupStats->n_direct_connections.end(); ++ii1) {
		const Gene* seedGene = ii1->first;
		const int seedGeneDirectConnections = ii1->second;

		tot_n_direct_connections += seedGeneDirectConnections;
		if (!_use_seed_permutation && _calc_genic_scores)
			stats["gene_" + groupPrefix + "direct_score|" + seedGene->getLabel()] = seedGeneDirectConnections;
	}
	stats[groupPrefix + "direct_edges"] = tot_n_direct_connections / 2.0; // undirected graph, so only count once here
	stats[groupPrefix + "direct_nodes"] = groupStats->direct_nodes.size();
	stats[groupPrefix + "direct_connectivity"] =
			groupStats->direct_nodes.size() == 0 ? 0 : tot_n_direct_connections / (double) groupStats->direct_nodes.size();

	// --- Indirect network seed gene connectivity ---
	if (_calc_indirect_connectivity) {
		double tot_n_indirect_connections = 0;
		for (GeneToInt::const_iterator ii2 = groupStats->n_indirect_connections.begin(); ii2 != groupStats->n_indirect_connections.end(); ++ii2) {
			const Gene* seedGene = ii2->first;
			const int seedGeneIndirectConnections = ii2->second;

			tot_n_indirect_connections += seedGeneIndirectConnections;
			if (!_use_seed_permutation && _calc_genic_scores)
				stats["gene_" + groupPrefix + "indirect_score|" + seedGene->getLabel()] = seedGeneIndirectConnections;
		}
		stats[groupPrefix + "indirect_edges"] = tot_n_indirect_connections / 2.0; // undirected graph, so only count once here
		stats[groupPrefix + "indirect_nodes"] = groupStats->indirect_nodes.size();
		stats[groupPrefix + "indirect_connectivity"] =
				groupStats->indirect_nodes.size() == 0 ? 0 : tot_n_indirect_connections / (double) groupStats->indirect_nodes.size();
	}

	// ---- Common interactors ----
	if (_calc_common_interactors) {
		int interactor_edge_count = 0;

		for (GeneToInt::const_iterator ii3 = groupStats->interactor_count.begin(); ii3 != groupStats->interactor_count.end(); ++ii3) {
			const Gene* CIgene = ii3->first;
			const int numConnectedSeeds = ii3->second;

			if (!_use_seed_permutation && _calc_genic_scores)
				stats["gene_" + groupPrefix + "interactor|" + CIgene->getLabel()] = numConnectedSeeds;
			interactor_edge_count += numConnectedSeeds;
		}
		stats[groupPrefix + "interactor_edges"] = interactor_edge_count;
		stats[groupPrefix + "interactor_nodes"] = groupStats->interactor_count.size();
		stats[groupPrefix + "interactor_connectivity"] =
				groupStats->interactor_count.size() == 0 ? 0 : interactor_edge_count / (double) groupStats->interactor_count.size();
	}
}

const std::string NetFunc::Net::DELTA_STATS_ARRAY[] = {
		"all_connectivity",
		"direct_connectivity",
		"indirect_connectivity",
		"interactor_connectivity",
		"direct_edges",
		"indirect_edges",
		"interactor_edges",
		"direct_nodes",
		"indirect_nodes",
		"interactor_nodes"};
std::list<std::string> NetFunc::Net::DELTA_STATS = arrayToList(NetFunc::Net::DELTA_STATS_ARRAY);

NetFunc::Net::NameToVal NetFunc::Net::netstats_full(KeyKeyValMap* out) const {
	// 1. Primary seed list (always)
	//  and if optionally a cseed list is given:
	// 2. C-seed list
	// 3. P-C delta stats (for each non-genic(?) measure, are seeds > cseeds )
	// 4. P-C connectivity (do seeds and cseeds have more (in)direct connections than expected?)

	NameToVal calcStats;

	//
	// TODO: Do we want to be using the same "out" for seeds and cseeds???
	// TODO: Discuss with Shaun...
	//

	//
	// --- Primary seed list ---
	//
	SingleSeedGroupStats seedsStats;
	netstats_singleSeedGroup(&_seeds, out, &seedsStats);
	addSingleSeedGroupStats(calcStats, &seedsStats, 1);

	//
	// --- Optionally, second comparator seed list ---
	//
	if (has_cseed_list()) {
		SingleSeedGroupStats cseedsStats;
		netstats_singleSeedGroup(&_cseeds, out, &cseedsStats);
		addSingleSeedGroupStats(calcStats, &cseedsStats, 2);

		/* Calculate some key delta scores, to be evaluated by permutation
		 */
		for (std::list<std::string>::const_iterator statIt = DELTA_STATS.begin(); statIt != DELTA_STATS.end(); ++statIt) {
			const std::string& statName = *statIt;
			// TODO : consider A-B, vs. using A/(A+B) against (A-B)/(A+B) :
			calcStats["delta_" + statName] = calcStats["1_" + statName] - calcStats["2_" + statName];
		}

		// ---- finally, calculate some measures of relatedness BETWEEN the seed and cseed list
		//      Specifically: do these have more direct/indirect connections than expected by chance?
		//      what are the common interactor genes that connect them?

		// how many connections between seeds and cseeds?
		int n_cross_self_connections = 0; // i.e. gene on both seed and cseed lists (meaningful in seed-permutation case, perhaps?)
		int n_cross_direct_connections = 0; // SEED is directly connected to CSEED
		int n_cross_indirect_connections = 0;

		// look for direct cross connections:
		for (Group::MemberWeightMap::const_iterator s1 = _seeds.memberWeightBegin(); s1 != _seeds.memberWeightEnd(); ++s1) {
			const Gene* seedGene = s1->first;

			// a "self-connection":
			if (isCseed(seedGene)) {
				++n_cross_self_connections;
				if (out)
					(*out)["cross-self"][seedGene->getLabel() + " <---> " + seedGene->getLabel()] = ".";
			}

			// get actual node that corresponds to this gene
			const Node* seedNode = _geneToNode.find(const_cast<Gene*>(seedGene))->second;

			for (Node::Neighbors::const_iterator neighbIt = seedNode->neighbBegin(); neighbIt != seedNode->neighbEnd(); ++neighbIt) {
				const Node* neighbcSeed = neighbIt->getToNode();
				/* 1. Exclude self-self interaction (which is probably excluded from the network to begin with...)
				 * 2. Require neighbcSeed to actually be a cSeed
				 */
				if (neighbcSeed == seedNode || !isCseed(neighbcSeed))
					continue;
				// There exists a directed edge: seedNode -> neighbCseed:

				// option --allow-cseed-cseed-in-cross relaxes the constraint below,
				//   i.e., will include SET-SET interactions as well as SET-NONSET interactions
				if (_include_cseed_cseed_OR_seed_seed_connections_in_cross || ((!isCseed(seedNode)) && (!isSeed(neighbcSeed)))) {
					++n_cross_direct_connections;
					if (out)
						(*out)["cross-direct"][seedGene->getLabel() + " <---> " + neighbcSeed->getGene()->getLabel()] = ".";
				}
			}
		}

		// Now, are there any shared interactors?

		// TODO: fix for directed graphs (SINCE THE FOLLOWING CODE ASSUMES THAT WE ARE DEALING WITH UNDIRECTED EDGES; WOULD NEED TO HAVE c/seedsStats.interactors in both directions, for example...)
		/*
		 * AS A REMINDER:
		 * seedsStats.interactors  = a map of: gene -> set of seed  genes it interacts with
		 * cseedsStats.interactors = a map of: gene -> set of cseed genes it interacts with
		 */
		for (GeneToGenes::const_iterator geneInteractIt = seedsStats.interactors.begin(); geneInteractIt != seedsStats.interactors.end(); ++geneInteractIt) {
			const Gene* CI = geneInteractIt->first;
			/* Want to consider a common interactor between seeds and cseeds:
			 * seed <---> CI <---> cseed
			 * where CI itself is not a seed or a cseed.
			 *
			 * The following condition makes a self-self interaction impossible in the above chain, since CI won't be a seed or a cseed:
			 */
			if (isSeed(CI) || isCseed(CI))
				continue;

			const ConstGeneSet& seedsInteractingWithCI = geneInteractIt->second;

			GeneToGenes::const_iterator findCIconnectedToCseeds = cseedsStats.interactors.find(CI);
			if (findCIconnectedToCseeds != cseedsStats.interactors.end()) { // CI is also connected to some cseed
				const ConstGeneSet& cseedsInteractingWithCI = findCIconnectedToCseeds->second;

				const Group* excludeCseeds = NULL;
				if (!_include_cseed_cseed_OR_seed_seed_connections_in_cross)
					excludeCseeds = &_cseeds;
				int nSeedsInteractingWithCI = numGenesNotInGroup(seedsInteractingWithCI, excludeCseeds);

				const Group* excludeSeeds = NULL;
				if (!_include_cseed_cseed_OR_seed_seed_connections_in_cross)
					excludeSeeds = &_seeds;
				int nCseedsInteractingWithCI = numGenesNotInGroup(cseedsInteractingWithCI, excludeSeeds);

				//
				// TODO: here we are counting paths, which is consistent with the indirect connection counting above. But, perhaps we want to count pairs of (seed, cseed) that are connected due to a CI?
				//
				// Multiply, since N seeds and M cseeds form N*M paths between seeds and cseeds (all via the one CI node):
				n_cross_indirect_connections += (nSeedsInteractingWithCI * nCseedsInteractingWithCI);

				if (out) {
					std::stringstream ss;

					for (ConstGeneSet::const_iterator ss1 = seedsInteractingWithCI.begin(); ss1 != seedsInteractingWithCI.end(); ++ss1) {
						if (ss1 != seedsInteractingWithCI.begin())
							ss << ",";
						ss << (*ss1)->getLabel();
					}
					ss << " <---> " << CI->getLabel() << " <---> ";

					for (ConstGeneSet::const_iterator ss1 = cseedsInteractingWithCI.begin(); ss1 != cseedsInteractingWithCI.end(); ++ss1) {
						if (ss1 != cseedsInteractingWithCI.begin())
							ss << ",";
						ss << (*ss1)->getLabel();
					}

					(*out)["cross-indirect"][CI->getLabel()] = ss.str();
				}
			}
		}

		calcStats["cross-self"] = n_cross_self_connections;
		calcStats["cross-direct"] = n_cross_direct_connections;
		calcStats["cross-indirect"] = n_cross_indirect_connections;

	} // end of special cseed calculations

	return calcStats;
}

int NetFunc::Net::numGenesNotInGroup(const ConstGeneSet& genes, const Group* excludeGroup) {
	int numGenes = genes.size();

	if (excludeGroup != NULL) {
		for (ConstGeneSet::const_iterator gIt = genes.begin(); gIt != genes.end(); ++gIt) {
			if (excludeGroup->isMember(*gIt))
				--numGenes;
		}
	}

	return numGenes;
}

void NetFunc::Net::random_draw(std::vector<int>& a) {
	// Generate a random permutation of 0 to n-1 where n is a.size(),
	// using Fisher-Yates shuffle, simultaneously initializing from
	// 0..n-1 and shuffling

	const int n = a.size();
	for (int i = 0; i < n; i++)
		a[i] = i;

	int tmp;
	for (int i = n; i > 1; i--) {
		int j = CRandom::rand(i);
		tmp = a[i - 1];
		a[i - 1] = a[j];
		a[j] = tmp;
	}
}

void NetFunc::Net::update_statistics
(const NameToVal& stats, const NameToVal& permd,
		NameToVal* expected, NameToVal* pvals) {
	// Note -- unlike many cases, here if we do not observe a value for the
	// original statistic/gene in the permed set, we count that as a permd
	// value that does not exceed the original.

	const double EPS = 1e-12;
	for (NameToVal::const_iterator ii = stats.begin(); ii != stats.end(); ++ii) {
		const std::string& statName = ii->first;
		const double obsStat = ii->second;

		NameToVal::const_iterator ff = permd.find(statName);
		if (ff != permd.end()) {
			const double permStat = ff->second;
			if (!Helper::realnum(permStat))
				std::cerr << statName << " is not real\n";

			if (permStat > obsStat) {
				++(*pvals)[statName];
			}
			else if (fabs(permStat - obsStat) < EPS) {
				// for ties, a coin-toss
				if (CRandom::rand() < 0.5)
					++(*pvals)[statName];
			}

			(*expected)[statName] += permStat;
		}
		else {
			// ++(*pvals)[ statName ];
		}
	}
}

NetFunc::Net::BinToStats NetFunc::Net::seed_bin_stats(NetFunc::Net::BinToLabel* labels) {
	labels->clear();

	BinToStats s;

	std::map<int, int> bincounts;
	std::map<int, int> seedcounts;

	std::map<const Node*, int> node2bin;
	for (DegreeToNodes::const_iterator jj = _degreeStats->merged_degree2node().begin(); jj != _degreeStats->merged_degree2node().end(); ++jj) {
		const int degBinInd = jj->first;
		const NodeVec& d = jj->second;
		//      const int n = d.size();

		for (NodeVec::const_iterator kk = d.begin(); kk != d.end(); ++kk) {
			const Node* node = *kk;
			// for this, we don't care if seed or cseed, so just pool both
			// (also fine if a gene on both lists, we only need to display this
			// summary output once in any case)

			if (isSeed(node) || isCseed(node)) {
				// this seed node belongs to this bin
				node2bin[node] = degBinInd;

				// and record that this bin has one more seed
				seedcounts[degBinInd]++;
			}

			// and denominator for this bin
			bincounts[degBinInd]++;
		}
	}

	// -- compile into the output that we want to display --
	for (std::map<const Node*, int>::const_iterator ll = node2bin.begin(); ll != node2bin.end(); ++ll) {
		const std::string& seedGeneName = ll->first->getGene()->getLabel();
		// this node belongs to bin
		const int binInd = ll->second;

		// N seeds, N total
		std::vector<double> t(2);
		t[0] = seedcounts[binInd];
		t[1] = bincounts[binInd];

		s[seedGeneName] = t;
		(*labels)[seedGeneName] = _degreeStats->merged_degree_labels().find(binInd)->second;
	}

	return s;
}

Data::Matrix<double> NetFunc::Net::Influence_Matrix(const double gamma) const {
	Data::Matrix<double> L = Laplacian();
	for (int i = 0; i < L.dim1(); i++)
		L(i, i) += gamma;
	return Statistics::inverse(L);
}

std::vector<std::string> NetFunc::Net::node_labels() const {
	std::vector<std::string> l(_nodes.size());
	int i = 0;

	for (Net::NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii)
		l[i++] = (*ii)->getGene()->getLabel();

	return l;
}

//http://compprog.wordpress.com/2008/01/17/finding-all-paths-of-minimum-length-to-a-node-using-dijkstras-algorithm/
//http://rosettacode.org/wiki/Dijkstra%27s_algorithm#C.2B.2B

void NetFunc::Net::DijkstraComputePaths(const Node* source,
		std::vector<double>& min_distance,
		std::vector<ConstNodeVec>& previous) const {
	const double max_weight = std::numeric_limits<double>::infinity();

	// number of nodes
	const int n = _nodes.size();

	// clear what will be output
	min_distance.clear();
	min_distance.resize(n, max_weight);
	min_distance[source->getNodeInd()] = 0; // using node's numeric (0..(n-1) identity)

	previous.clear();
	previous.resize(n); // i.e. a 'NULL' size 0 vector for each node initially

	std::cout << "source = " << source->getGene()->getLabel() << " " << source->getNodeInd() << "\n";

	typedef std::pair<double, const Node*> DistNode;
	std::set<DistNode> vertex_queue;
	vertex_queue.insert(DistNode(min_distance[source->getNodeInd()], source));

	while (!vertex_queue.empty()) {
		double dist = vertex_queue.begin()->first;
		const Node* u = vertex_queue.begin()->second;
		vertex_queue.erase(vertex_queue.begin());

		std::cout << "considering u = " << u->getGene()->getLabel() << "\n";

		// Visit each edge exiting u
		for (Node::Neighbors::const_iterator neighbIt = u->neighbBegin(); neighbIt != u->neighbEnd(); ++neighbIt) {
			const Node* v = neighbIt->getToNode();
			double weight = neighbIt->getWeight();

			std::cout << "adding wgt " << neighbIt->getWeight() << " for " << u->getGene()->getLabel() << "-" << v->getGene()->getLabel() << "\n";

			double distance_through_u = dist + weight;

			// change to make a set of shortest paths
			if (distance_through_u < min_distance[v->getNodeInd()]) {
				vertex_queue.erase(DistNode(min_distance[v->getNodeInd()], v));
				min_distance[v->getNodeInd()] = distance_through_u;
				previous[v->getNodeInd()].clear();
				previous[v->getNodeInd()].push_back(u);
				vertex_queue.insert(DistNode(min_distance[v->getNodeInd()], v));
			}
			else if (distance_through_u == min_distance[v->getNodeInd()]) { // equally short path?
				// add another shortest path
				previous[v->getNodeInd()].push_back(u);
			}
		}
	}
}

std::vector<NetFunc::Net::ConstNodeList> NetFunc::Net::DijkstraGetShortestPaths
(const Node* vertex, const std::vector<ConstNodeVec>* previous) const {
	std::vector<ConstNodeList> paths;
	DijkstraGetShortestPathsHelper(vertex, ConstNodeList(), &paths, previous);
	return paths;
}

void NetFunc::Net::DijkstraGetShortestPathsHelper(const Node* vertex,
		ConstNodeList path, std::vector<ConstNodeList>* paths,
		const std::vector<ConstNodeVec>* previous) const {

	path.push_back(vertex);
	const int v = vertex->getNodeInd();
	const ConstNodeVec& prev = (*previous)[v];
	const int np = prev.size();

	if (np == 0) {
		paths->push_back(path);
		return;
	}

	for (int i = 0; i < np; i++)
		DijkstraGetShortestPathsHelper(prev[i], path, paths, previous);
}

NetFunc::paths_t NetFunc::Net::Dijkstra(Node* a) const {
	paths_t paths;

	// for each node, shortest distance (weighted)
	std::vector<double> min_distance;

	// store all possible shortest paths
	std::vector<ConstNodeVec> previous;

	DijkstraComputePaths(a, min_distance, previous);

	const int n = _nodes.size();
	for (int i = 0; i < n; i++) {
		std::vector<NetFunc::Net::ConstNodeList> paths = DijkstraGetShortestPaths(_nodes[i], &previous);

		std::cout << "distance from " << a->getGene()->getLabel() << " to " << _nodes[i]->getGene()->getLabel() << "\t" << min_distance[i] << "\n";

		for (int p = 0; p < paths.size(); p++) {
			std::cout << "\tpath" << p << "\t";
			for (ConstNodeList::const_iterator ii = paths[p].begin(); ii != paths[p].end(); ++ii)
				std::cout << " " << (*ii)->getGene()->getLabel();

			std::cout << "\n";
		}
	}

	return paths;
}

const NetFunc::Net::EquivNodesList* NetFunc::Net::calc_connected_components() {
	if (_components != NULL)
		delete _components;

	DisjointSet<const Node*> V;

	for (NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii) {
		const Node* nd = *ii;
		for (Node::Neighbors::const_iterator jj = nd->neighbBegin(); jj != nd->neighbEnd(); ++jj) {
			if (V.findSetRep(nd) != V.findSetRep(jj->getToNode()))
				V.setUnion(nd, jj->getToNode());
		}
	}
	_components = V.getCurrentEquivSets();
	return _components;
}

void NetFunc::Net::create_subnetwork(double delta) {
	for (NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii) {
		Node* node = *ii;
		const int n1 = node->getNodeInd();
		for (Node::Neighbors::const_iterator neighbIt = node->neighbBegin(); neighbIt != node->neighbEnd(); ++neighbIt) {
			const Edge& e = *neighbIt;
			const Node* neighb = e.getToNode();

			if (e.getWeight() < delta)
				removeDirectedEdge(n1, neighb->getNodeInd());
		}
	}
}

void NetFunc::Net::create_network_from_matrix(const Data::Matrix<double>& m, const double thr, const bool greater_than) {
	// First, clear all connections
	removeAllEdges();

	// We expect the matrix to be in the same order as nodes:
	const int nrow = m.dim1();
	const int ncol = m.dim2();

	for (int c = 0; c < ncol; c++) {
		for (int r = 0; r < nrow; r++) {
			if ((greater_than && m(r, c) >= thr) || (!greater_than && m(r, c) < thr))
				addUndirectedEdge(r, c, m(r, c));
		}
	}
}

void NetFunc::Net::remove_edges_based_on_matrix_vals(const Data::Matrix<double>& m, const double thr, bool remove_if_less_than) {
	// We expect the matrix to be in the same order as nodes:
	for (NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii) {
		Node* node = *ii;
		const int n1 = node->getNodeInd();
		for (Node::Neighbors::const_iterator neighbIt = node->neighbBegin(); neighbIt != node->neighbEnd(); ++neighbIt) {
			const Edge& e = *neighbIt;
			const Node* neighb = e.getToNode();
			const int n2 = neighb->getNodeInd();

			if ((remove_if_less_than && m(n1, n2) <= thr) || (!remove_if_less_than && m(n1, n2) > thr))
				removeDirectedEdge(n1, n2);
		}
	}
}

// For HotNet, remove edges with score below threshold thr [score == the sum (or max) of the two genic scores times value in the matrix]:
void NetFunc::Net::remove_edges_from_matrix_seed_score_enhanced(const Data::Matrix<double>& m, const double thr) {
	// We expect the matrix to be in the same order as nodes:
	for (NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii) {
		Node* node = *ii;
		const int n1 = node->getNodeInd();
		for (Node::Neighbors::const_iterator neighbIt = node->neighbBegin(); neighbIt != node->neighbEnd(); ++neighbIt) {
			const Edge& e = *neighbIt;
			const Node* neighb = e.getToNode();
			const int n2 = neighb->getNodeInd();
			const double u = m(n1, n2);

			// will be 0 if not a seed
			// otherwise, assume -2*log(p)
			const double e1 = _seeds.getWeight(node->getGene());
			const double e2 = _seeds.getWeight(neighb->getGene());

			// max:
			//const double w = e1 > e2 ? u * e1 : u * e2 ;

			// sum:
			const double w = u * (e1 + e2);

			if (w < thr)
				removeDirectedEdge(n1, n2);
		}
	}
}

NetFunc::Net::ConnComp NetFunc::Net::get_connected_components_of_size(int s, int t) {
	if (t == -1)
		t = s;

	ConnComp r;
	if (_components == NULL)
		calc_connected_components();

	for (Net::EquivNodesList::const_iterator ii = _components->begin(); ii != _components->end(); ++ii) {
		const Net::EquivNodes& cc = ii->second;
		if (cc.size() >= s && cc.size() <= t) {
			std::set<std::string> rr;

			for (Net::EquivNodes::const_iterator jj = cc.begin(); jj != cc.end(); ++jj) {
				const Node* node = *jj;
				// if ( node == NULL ) std::cout << "node is null\n";
				// if ( node->gene == NULL ) std::cout << "gene is null\n";
				// std::cout << node->gene->getLabel() << " is label\n";
				rr.insert(node->getGene()->getLabel());
			}
			r.push_back(rr);
		}
	}

	return r;
}
