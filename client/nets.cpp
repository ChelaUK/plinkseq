#include "plinkseq.h"
#include "netassoc.h"
#include "util.h"
#include "plinkseq/network.h"

#include <cmath>
#include <limits.h>

extern GStore g;
extern Pseq::Util::Options args;

//
// TODO
//

//  TODO   locus representation

//  NO?    1/N connectivity weighting
//  NO?    edge permutation
//  DONE   2 seed list

//  ??     weighted nodes
//  TO COMPLETE  weighted edges
//  IN PROGRESS  influence graph enrichment analysis (Vandin et al)  HotNet

struct aux_nets_t {
	aux_nets_t() {
		net = NULL;

		use_background_list = false;

		use_edge_permutation = false;

		use_seed_permutation = false;
		seed_perm_file = NULL;
		std::string permed_seedlist = "";

		NREP = 0;
	}

	NetFunc::Net* net;

	bool use_background_list;
	std::set<std::string> background_genes;

	bool use_edge_permutation;

	bool use_seed_permutation;
	InFile* seed_perm_file;
	std::string permed_seedlist;

	int NREP;
};

bool Pseq::NetFunc::setup_net(aux_nets_t& aux) {
	// attach NETDB
	if (!args.has("netdb"))
		Helper::halt("no --netdb specified");

	NetDBase netdb;

	netdb.attach(args.as_string("netdb"));

	if (!netdb.attached())
		Helper::halt("no attached NETDB");

	//
	// Options
	//
	aux.use_seed_permutation = args.has("seed-perm");

	if (aux.use_seed_permutation)
		aux.permed_seedlist = args.as_string("seed-perm");

	aux.use_edge_permutation = args.has("edge-perm");

	if (aux.use_edge_permutation)
		Helper::halt("edge-permutation currently not implemented");

	// exclude list?
	aux.use_background_list = args.has("background");

	if (aux.use_background_list) {
		std::string d = args.as_string("background");
		std::string d2 = Helper::filelist2commalist(d);
		std::vector<std::string> t = Helper::parse(d2, ",");
		for (int s = 0; s < t.size(); s++)
			aux.background_genes.insert(t[s]);
		plog << "Using gene background list of " << aux.background_genes.size() << " genes\n";
	}

	bool has_exclude_from_net_list = args.has("exclude-from-net");
	std::set<std::string> exclude_from_net_list;
	if (has_exclude_from_net_list) {
		std::string d = args.as_string("exclude-from-net");
		std::string d2 = Helper::filelist2commalist(d);
		std::vector<std::string> t = Helper::parse(d2, ",");
		for (int s = 0; s < t.size(); s++)
			exclude_from_net_list.insert(t[s]);
		plog << "Using an exclude-list for the network, of " << exclude_from_net_list.size() << " genes\n";
	}

	//
	// Merge degree bins?
	//
	int merge_degree = -1; // default == complete degree matching

	if (args.has("no-degree-matching")) {
		if (aux.use_seed_permutation)
			Helper::halt("using seed permutation, so not valid to specify degree-matching options");

		merge_degree = 0;
	}
	else if (args.has("degree-merge-bin")) {
		if (aux.use_seed_permutation)
			Helper::halt("not valid to specify bin-size for node-permutation if using seed-permutation");

		merge_degree = args.as_int("degree-merge-bin");

		if (merge_degree == 0)
			plog << "No within-degree matching for node-permutation\n";
		else if (merge_degree > 0)
			plog << "Binning degrees to have " << merge_degree << " nodes per bin\n";
		else
			Helper::halt("invalid degree-merge-bin");
	}

	//
	// Create network
	//
	aux.net = new ::NetFunc::Net(&netdb, args.has("edge-weights"),
			has_exclude_from_net_list ? &exclude_from_net_list : NULL);

	aux.net->edge_permutation(aux.use_edge_permutation);

	aux.net->degree_merge_threshold(merge_degree);

	return true;
}

bool Pseq::NetFunc::check_node_binning(Out& pout, aux_nets_t& aux) {
	::NetFunc::Net::BinToLabel bin_label;
	::NetFunc::Net::BinToStats sbstats = aux.net->seed_bin_stats(&bin_label);

	for (::NetFunc::Net::BinToStats::iterator sbi = sbstats.begin(); sbi != sbstats.end(); ++sbi) {
		const double nseeds = (sbi->second)[0];
		const double ntotal = (sbi->second)[1];
		pout << "seed_bin_stats" << "\t" << sbi->first << "\t" << aux.net->degree(sbi->first) << "\t" // Actual binding degree of gene
		<< bin_label[sbi->first] << "\t" // Range of binned degrees
		<< nseeds << "\t" // # of seeds in this binding-degree bin
		<< ntotal << "\t" // # of genes in this binding-degree bin
		<< nseeds / ntotal << "\n"; // ratio
	}

	return true;
}

//
// TODO: IMPLEMENT LOCUS FOR SEEDS!!
//
bool Pseq::NetFunc::read_seed_groups(aux_nets_t& aux) {
	Out& pout = Out::stream("nets");

	//
	// Seed gene list?
	//
	if (args.has("seeds"))
		read_seed_file(aux, args.as_string("seeds"), "seed", &::NetFunc::Net::addSeed, pout);

	//
	// Comparator seeds (cseeds) list?
	//
	if (args.has("cseeds"))
		read_seed_file(aux, args.as_string("cseeds"), "cseed", &::NetFunc::Net::addCseed, pout);

	//
	// At this point, require some seed genes to be specified
	//
	if (!aux.use_seed_permutation) {
		if (aux.net->n_seeds() == 0)
			Helper::halt("no --seeds specified, but not using --seed-perm");
		else
			plog << aux.net->n_seeds() << " seed genes in the network\n";
	}

	return true;
}

// Format (3-col tab) : LOCUS/GROUP  SEED-GENE  WGT
bool Pseq::NetFunc::read_seed_file(aux_nets_t& aux, std::string seedFileName, std::string seedClassName, addGeneLocusToGroupFunc addSeedFunc, Out& pout) {
	bool added = false;

	InFile seedFile(seedFileName);
	while (!seedFile.eof()) {
		std::vector<std::string> tok = seedFile.tokenizeLine("\t");
		// skip comments and empty lines
		if (tok.size() == 0)
			continue;
		if (tok[0].substr(0, 1) == "#")
			continue;

		std::string gene;
		double wgt = 1;
		std::string grp = "";

		if (tok.size() == 1)
			gene = tok[0];
		else if (tok.size() == 3) {
			// GROUP GENE WGT
			grp = tok[0];
			gene = tok[1];
			wgt = Helper::str2dbl(tok[2]);
			// -2ln(p) encoding
			if (wgt <= 0)
				wgt = 1e-22;
			wgt = -2 * log(wgt);
			if (wgt <= 0)
				wgt = 0;
		}
		else
			Helper::halt("requires wither a simple 1-col gene list or a 3 tab-delimited columns");

		//
		// TODO
		//
		// currently 'group' is not used -- this will represent the locus grouping when
		// that is implemented
		if ((!aux.use_background_list) || aux.background_genes.find(gene) != aux.background_genes.end())
			if (!(aux.net->*addSeedFunc)(gene, wgt))
				pout << "missing_" << seedClassName << "\t" << gene << "\n";
			else
				added = true;
	}
	seedFile.close();

	return added;
}

bool Pseq::NetFunc::init_seed_perm_file(aux_nets_t& aux) {
	if (!aux.use_seed_permutation) {
		aux.seed_perm_file = NULL;
		return false;
	}

	aux.seed_perm_file = new InFile(aux.permed_seedlist);
	if (aux.seed_perm_file->bad() || aux.seed_perm_file->eof()) {
		Helper::halt("problem opening seed-list: " + aux.permed_seedlist);
	}

	// read original seeds
	if (!setSeedsFromNextLineInSeedPermFile(aux, 0))
		Helper::halt("no seeds read from seed-list");

	return true;
}

/* Assume FORMAT:
 * RUNTYPE P CONTRAST SAMPLE GENE1 GENE2 GENE3...
 *
 * P==0  original data
 * P>0   all permutations (should have same # of loci as original)
 */
#define FIRST_GENE_INDEX 4

bool Pseq::NetFunc::setSeedsFromNextLineInSeedPermFile(aux_nets_t& aux, int permNum) {
	std::vector<std::string> tok = Helper::parse(aux.seed_perm_file->readLine(), "/\t");
	if (tok.size() == 0)
		return false;

	if (tok.size() < (FIRST_GENE_INDEX + 1))
		Helper::halt("need at least one seed, and expecting 4 columns first");

	if (permNum != -1) {
		int pnum = Helper::str2int(tok[1]);
		if (pnum != permNum) {
			stringstream str;
			str << "expecting permutation number " << permNum;
			Helper::halt(str.str());
		}
	}
	aux.net->clearSeeds();

	for (int g = FIRST_GENE_INDEX; g < tok.size(); ++g) {
		// TODO: IMPLEMENT LOCUS!!
		// For now, we only take the first gene if
		// multiple genes are hit by the same event (delimited by '/')
		std::string gene = tok[g];
		if (gene.find("/") != std::string::npos)
			gene = gene.substr(0, gene.find("/"));

		if ((!aux.use_background_list) || aux.background_genes.find(gene) != aux.background_genes.end())
			aux.net->addSeed(gene);
	}

	return true;
}

bool Pseq::NetFunc::permute(aux_nets_t& aux) {
	if (aux.use_seed_permutation)
		return setSeedsFromNextLineInSeedPermFile(aux);
	else
		aux.net->permuteNodeGeneRelationships();

	return true;
}

bool Pseq::NetFunc::view_netnodes() {
	Out& pout = Out::stream("nodes");
	aux_nets_t aux;
	setup_net(aux);

	std::vector<std::string> nodes = aux.net->node_labels();
	for (int i = 0; i < nodes.size(); i++)
		pout << nodes[i] << "\n";

	return true;
}

bool Pseq::NetFunc::dump_Laplacian() {
	Out& pout = Out::stream("laplacian");
	aux_nets_t aux;
	setup_net(aux);

	Data::Matrix<double> L = aux.net->Laplacian();
	for (int r = 0; r < L.dim1(); r++) {
		for (int c = 0; c < L.dim2(); c++) {
			if (c)
				pout << "\t";
			pout << L(r, c);
		}
		pout << "\n";
	}

	return true;
}

bool Pseq::NetFunc::delta_hotnet() {
	// expecting a seed list with some large-ish number of groups,
	// that contains (e.g. 100) versions of gene scores from the null
	// distribution.   This is used to choose the value(s) of delta
	// that give the largest number of s=3,4,5 subnetworks under the
	// null (selection done manually)

	// this uses a similar code base as below.

	if (!args.has("delta-min"))
		Helper::halt("no --delta-min specified");
	if (!args.has("delta-max"))
		Helper::halt("no --delta-max specified");
	if (!args.has("delta-step"))
		Helper::halt("no --delta-step specified");

	const double delta_min = args.as_float("delta-min");
	const double delta_max = args.as_float("delta-max");
	const double delta_step = args.as_float("delta-step");

	if (!args.has("null-seeds"))
		Helper::halt("no --null-seeds specified");

	InFile seedfile(args.as_string("null-seeds"));

	std::string last_grp = "";

	bool first = true;

	std::vector<std::string> genes;

	std::vector<std::vector<double> > null_scores;
	std::vector<double>* current = NULL;

	while (!seedfile.eof()) {
		std::vector<std::string> tok = seedfile.tokenizeLine("\t");

		// skip comments and empty lines
		if (tok.size() == 0)
			continue;
		if (tok[0].substr(0, 1) == "#")
			continue;
		if (tok.size() != 3)
			Helper::halt("expecting --null-seeds file to have three columns, GROUP GENE PVALUE");

		std::string& grp = tok[0];
		std::string& gene = tok[1];
		double wgt = Helper::str2dbl(tok[2]);

		if (grp != last_grp) { // a new set of null values
			if (last_grp != "")
				first = false;
			last_grp = grp;
			std::vector<double> t;
			null_scores.push_back(t);
			current = &(null_scores[null_scores.size() - 1]);
		}

		if (first)
			genes.push_back(gene);

		// -2ln(p) encoding
		if (wgt <= 0)
			wgt = 1e-22;
		wgt = -2 * log(wgt);
		if (wgt <= 0)
			wgt = 0;

		// add value
		current->push_back(wgt);
	}
	// now, null_scores should be e.g. 100 x

	// for (int i=0; i<null_scores.size(); i++)
	//   {
	//     std::cout << i << "\t" << genes.size() << "\t" << null_scores[i].size() << "\n";
	//     for (int j = 0 ; j < 10 ; j++ ) std::cout << " " << null_scores[i][j] ;
	//     std::cout << "\n";
	//   }

	//
	// Set-up NETDB
	//
	aux_nets_t aux;
	setup_net(aux);
	::NetFunc::Net*& net = aux.net;

	//
	// Influence matrix
	//
	if (!args.has("influence-matrix"))
		Helper::halt("need to specify an influence matrix with --influence-matrix");
	if (!args.has("matdb"))
		Helper::halt("no MATDB specified");
	std::string name = args.as_string("matdb");

	MatDBase matdb;
	matdb.attach(name);
	if (!matdb.attached())
		Helper::halt("problem attaching MATDB");
	Data::Matrix<double> I = matdb.read(args.as_string("influence-matrix"));
	if (I.dim1() != I.dim2())
		Helper::halt("problem, influence matrix is not square");
	plog << "read influence matrix, for " << I.dim1() << " genes\n";

	//
	// Go through null seeds
	//
	int n_nodes, n_edges, n_nodes_in_network;
	net->basic_n(&n_nodes, &n_edges, &n_nodes_in_network);

	Out& pout = Out::stream("hotnet.delta");

	// header
	pout << "REP\tDELTA\tN_NODES\tN_EDGES\tCC";
	for (int i = 2; i <= 10; i++)
		pout << "\tN_CC_EQ" << i;
	for (int i = 2; i <= 10; i++)
		pout << "\tN_CC_GE" << i;
	pout << "\n";

	// first row: output for original data (i.e. delta = 0 )

	// get connected components
	net->calc_connected_components();
	pout << "0\t0\t" << n_nodes << "\t" << n_edges << "\t" << net->n_connected_components();

	// count connected components
	for (int s = 2; s <= 10; s++) { // of size s exactly
		std::vector<std::set<std::string> > subnets = net->get_connected_components_of_size(s);
		pout << "\t" << subnets.size();
	}
	for (int s = 2; s <= 10; s++) { // of s or more
		std::vector<std::set<std::string> > subnets = net->get_connected_components_of_size(s, 99999);
		pout << "\t" << subnets.size();
	}
	pout << "\n";

	net->clear_connected_components();

	//
	// Now consider the null replicates
	//
	for (int i = 0; i < null_scores.size(); i++) {
		plog.counter2("processing replicate " + Helper::int2str(i));

		net->clearSeeds();
		if (null_scores[i].size() != genes.size())
			Helper::halt("problem with null scores sizes");

		for (int j = 0; j < genes.size(); j++)
			net->addSeed(genes[j], null_scores[i][j]);

		for (double delta = delta_min; delta <= delta_max; delta += delta_step) {
			::NetFunc::Net* netCopy = new ::NetFunc::Net(*net);

			// get the enriched network
			netCopy->remove_edges_from_matrix_seed_score_enhanced(I, delta);

			// get connected components
			netCopy->calc_connected_components();

			// other basic stats / sanity check
			int n_nodes, n_edges, n_nodes_in_network;
			netCopy->basic_n(&n_nodes, &n_edges, &n_nodes_in_network);

			// get connected components
			pout << i << "\t" << delta << "\t" << n_nodes << "\t" << n_edges << "\t" << netCopy->n_connected_components();
			for (int s = 2; s <= 10; s++) {
				std::vector<std::set<std::string> > subnets = netCopy->get_connected_components_of_size(s);
				pout << "\t" << subnets.size();
			}

			for (int s = 2; s <= 10; s++) {
				std::vector<std::set<std::string> > subnets = netCopy->get_connected_components_of_size(s, 99999);
				pout << "\t" << subnets.size();
			}
			pout << "\n";

			delete netCopy;
		}
	}

	return true;
}

bool Pseq::NetFunc::calc_hotnet() {
	Out& pout = Out::stream("hotnet");
	aux_nets_t aux;

	//
	// Attach NETDB
	//
	setup_net(aux);

	// convenience
	::NetFunc::Net*& net = aux.net;
	::NetFunc::Net* origNet = net;

	//
	// Read a (required) weighted seed list
	//
	read_seed_groups(aux);

	//
	// Read influence matrix from a matdb
	//
	if (!args.has("influence-matrix"))
		Helper::halt("need to specify an influence matrix with --influence-matrix");

	if (!args.has("matdb"))
		Helper::halt("no MATDB specified");
	std::string name = args.as_string("matdb");

	std::cerr << "attaching influence matrix...\n";

	MatDBase matdb;
	matdb.attach(name);
	if (!matdb.attached())
		Helper::halt("problem attaching MATDB");

	Data::Matrix<double> I = matdb.read(args.as_string("influence-matrix"));

	if (I.dim1() != I.dim2())
		Helper::halt("problem, influence matrix is not square");
	plog << "read influence matrix, for " << I.dim1() << " genes\n";

	//
	// Using node permutation here: check that node-binning seems okay
	//
	check_node_binning(pout, aux);

	//
	// Pruning factor network (determined by hotnet-delta)
	//
	if (!args.has("delta"))
		Helper::halt("no --delta specified");

	const double delta = args.as_float("delta");

	//
	// Prune network
	//
	::NetFunc::Net* netCopy = new ::NetFunc::Net(*net);
	net = netCopy;
	net->remove_edges_from_matrix_seed_score_enhanced(I, delta);

	//
	// Original statistics
	//

	// output: (type of test/gene/set/value)
	::NetFunc::Net::KeyKeyValMap output;
	::NetFunc::Net::NameToVal orig = stat_hotnet(aux, &output);

	//
	// Clean-up
	//
	net = origNet;
	delete netCopy;

	//
	// Set up permutations
	//
	::NetFunc::Net::NameToVal pvals;
	::NetFunc::Net::NameToVal expected;

	aux.NREP = args.has("perm") ? args.as_int("perm") : 1000;

	for (int r = 0; r < aux.NREP; r++) {
		std::cerr << "rep = " << r << "\n";
		if (r % 10 == 0)
			plog.counter2(Helper::int2str(r) + " permutations completed");

		//
		// Permute network (using node permutation)
		//
		bool completed = !permute(aux);
		if (completed)
			break;

		//
		// Recalculate HotNet statistics
		//
		::NetFunc::Net* netCopy = new ::NetFunc::Net(*net);
		net = netCopy;
		net->remove_edges_from_matrix_seed_score_enhanced(I, delta);

		::NetFunc::Net::NameToVal perm = stat_hotnet(aux);

		//
		// Set statistics
		//
		::NetFunc::Net::update_statistics(orig, perm, &expected, &pvals);

		//
		// Restore network
		//
		net = origNet;
		delete netCopy;
	}

	//
	// Display statistics, and p-values
	//

	//
	// (1) verbose output
	//
	for (::NetFunc::Net::KeyKeyValMap::const_iterator oo = output.begin(); oo != output.end(); ++oo) {
		for (::NetFunc::Net::KeyValMap::const_iterator pp = oo->second.begin(); pp != oo->second.end(); ++pp) {
			pout << oo->first << "\t" << pp->first << "\t" << pp->second << "\n";
		}
	}

	//
	// (2) network statistics
	//
	pout << "network stats\n";
	for (::NetFunc::Net::NameToVal::const_iterator kk = orig.begin(); kk != orig.end(); ++kk)
		pout << "stats_" << kk->first << "\t" << kk->second << "\t" << expected[kk->first] / (double) aux.NREP << "\t" << (pvals[kk->first] + 1.0) / (double) (aux.NREP + 1.0) << "\n";

	// // For real data, display the genes in the significant components:

	// for (int s=1;s<20;s++)
	//   {
	//     std::vector<std::set<std::string> > subnets = net->get_connected_components_of_size( s );
	//     std::cout << "---------------------------subnet of size " << s << "\n";
	//     for (int i=0;i<subnets.size();i++)
	// 	{
	// 	  std::cout << "\n";
	// 	  std::set<std::string>::iterator ii = subnets[i].begin();
	// 	  while ( ii != subnets[i].end() )
	// 	    {
	// 	      std::cout << "\t" << *ii << "\n";
	// 	      ++ii;
	// 	    }
	// 	}
	//   }

	return true;
}

::NetFunc::Net::NameToVal Pseq::NetFunc::stat_hotnet(aux_nets_t& aux, ::NetFunc::Net::KeyKeyValMap* out) {
	::NetFunc::Net::NameToVal stats;

	// get connected components
	aux.net->calc_connected_components();

	const int smin = 2;
	const int smax = 50;

	bool genic_tests = true;

	std::set<std::string> added_gene;
	std::set<std::string> in_cc_gene;

	for (int s = smin; s <= smax; s++) {
		std::vector<std::set<std::string> > subnets = aux.net->get_connected_components_of_size(s, 99999);
		stats["S" + (s < 10 ? "0" + Helper::int2str(s) : Helper::int2str(s))] = subnets.size();
		std::cerr << "S" << s << "\t" << subnets.size() << "\n";
		if (out)
			(*out)["S"][Helper::int2str(s)] = Helper::int2str(subnets.size());

		for (int i = 0; i < subnets.size(); i++) {
			std::set<std::string>& subnet = subnets[i];
			std::set<std::string>::const_iterator ii = subnet.begin();
			while (ii != subnet.end()) {
				in_cc_gene.insert(*ii);
				stats["G_" + *ii] = 1;
				++ii;
			}
		}

		stats["G"] = in_cc_gene.size();

		if (out && s == 2) {
			for (int i = 0; i < subnets.size(); i++) {
				std::set<std::string>& subnet = subnets[i];
				std::cerr << "\n" << "SUBSET " << i + 1 << " S" << subnets[i].size();

				for (std::set<std::string>::const_iterator ii = subnet.begin(); ii != subnet.end(); ++ii)
					std::cerr << "\t" << *ii;
			}
		}

		//    if ( out ) (*out)[ "S" ][ Helper::int2str( s ) ] = Helper::int2str( subnets.size() );

		// if ( genic_tests )
		// 	{
		// 	  // go through all
		// 	  for (int i=0;i<subnets.size();i++)
		// 	    {
		// 	      std::set<std::string>& subnet = subnets[i];
		// 	      std::set<std::string>::const_iterator ii = subnet.begin();
		// 	      while ( ii != subnet.end() )
		// 		{
		// 		  if ( added_gene.find(*ii ) != added_gene.end() )
		// 		    {
		// 		    }

		// 		  ++ii;
		// 		}
		// 	      = aux.net->get_connected_components_of_size( s , 99999 );
		// 	}
	}

	// individual gene statistics?
	//  weight by presence in small set?
	//  let's see how many large sets are actually emerging?

	// for (int s=1;s<20;s++)
	//   {
	//     std::vector<std::set<std::string> > subnets = net->get_connected_components_of_size( s );
	//     std::cout << "---------------------------subnet of size " << s << "\n";
	//     for (int i=0;i<subnets.size();i++)
	// 	{
	// 	  std::cout << "\n";
	// 	  std::set<std::string>::iterator ii = subnets[i].begin();
	// 	  while ( ii != subnets[i].end() )
	// 	    {
	// 	      std::cout << "\t" << *ii << "\n";
	// 	      ++ii;
	// 	    }
	// 	}
	//   }

	// clean-up
	aux.net->clear_connected_components();

	return stats;
}

bool Pseq::NetFunc::calc_infl_matrix() {
	Out& pout = Out::stream("infmat");

	aux_nets_t aux;

	setup_net(aux);

	//
	// Get Laplacian
	//
	Data::Matrix<double> I = aux.net->Influence_Matrix();

	std::vector<std::string> genes = aux.net->node_labels();

	pout << genes.size();
	for (int g = 0; g < genes.size(); g++)
		pout << "\t" << genes[g];
	pout << "\n";

	for (int i = 0; i < I.dim1(); i++) {
		pout << genes[i];
		for (int j = 0; j < I.dim2(); j++)
			pout << "\t" << I(i, j);
		pout << "\n";
	}

	return true;
}

bool Pseq::NetFunc::calc_netstats() {
	// Requires a working RNG
	CRandom::srand(time(0));

	Out& pout = Out::stream("nets");

	aux_nets_t aux;

	setup_net(aux);

	::NetFunc::Net*& net = aux.net;

	//
	// Read a (required) seed list (and optionally a cseed list)
	//
	read_seed_groups(aux);

	//
	// If cseed mode, do we need to re-make the node bins?
	//
	if (net->has_cseed_list()) {
		// some auxilliary cseed options
		bool fix_cseed_node_perms = true;
		bool include_cseed_cseed_OR_seed_seed_connections_in_cross = false;

		if (args.has("permute-cseeds"))
			fix_cseed_node_perms = false;

		if (args.has("allow-cseed-cseed-or-seed-seed-in-cross"))
			include_cseed_cseed_OR_seed_seed_connections_in_cross = true;

		net->set_cseed_param(fix_cseed_node_perms, include_cseed_cseed_OR_seed_seed_connections_in_cross);
	}

	//
	// Switch off various optional net-statistics
	//
	if (args.has("no-ci"))
		net->calc_common_interactors(false);
	if (args.has("no-genic"))
		net->calc_genic_scores(false);
	if (args.has("no-general"))
		net->calc_general_connectivity(false);
	if (args.has("no-indirect"))
		net->calc_indirect_connectivity(false);

	double dd1 = -1;
	plog << "mean degree distribution across network " << net->degree_distribution(&dd1, NULL) << "\n";
	plog << "mean degree distribution for in-network nodes (excluding 0-degree nodes) " << dd1 << "\n";

	//
	// If use seed permutation, open filestream and read the original seed list
	// Note: currently, this method cannot be used to specify a cseed list also
	//       can add in future easily enough I imagine
	//
	init_seed_perm_file(aux);

	//
	// If using node permutation, flag any seeds that might not get properly permuted
	// by the specified node-permutation alone (i.e. assuming no edge permutation
	// right now)
	//
	if (!aux.use_seed_permutation)
		check_node_binning(pout, aux);

	//
	// Primary outputs, original data
	//
	::NetFunc::Net::KeyKeyValMap output;
	::NetFunc::Net::NameToVal stats = net->netstats_full(&output);

	//
	// Now permute network and recalculate net-stats
	//
	::NetFunc::Net::NameToVal pvals;
	::NetFunc::Net::NameToVal expected;

	aux.NREP = args.has("perm") ? args.as_int("perm") : 1000;
	if (aux.use_seed_permutation)
		// so that will keep permuting until no more seed permutations left in file
		aux.NREP = INT_MAX;

	for (int r = 0; r < aux.NREP; r++) {
		if ((r+1) % 100 == 0)
			plog.counter2(Helper::int2str((r+1)) + " permutations completed");

		//
		// Permute network (using either seed or node permutation)
		//
		bool completed = !permute(aux);
		if (completed)
			break;

		//
		// Now, for this permutation, recalculate the netstats
		//
		::NetFunc::Net::NameToVal perm = net->netstats_full();
		::NetFunc::Net::update_statistics(stats, perm, &expected, &pvals);

		// even if edge-permutation is enabled, this is probably not necessary
		//if ( use_edge_permutation ) net->restore();
	}

	//
	// All done; close the seed-permutation file if opened
	//
	if (aux.seed_perm_file) {
		aux.seed_perm_file->close();
		delete aux.seed_perm_file;
	}

	//
	// Display statistics, and p-values
	//

	//
	// (1) verbose output
	//
	for (::NetFunc::Net::KeyKeyValMap::const_iterator oo = output.begin(); oo != output.end(); ++oo) {
		for (::NetFunc::Net::KeyValMap::const_iterator pp = oo->second.begin(); pp != oo->second.end(); ++pp)
			pout << oo->first << "\t" << pp->first << "\t" << pp->second << "\n";
	}

	//
	// (2) network statistics
	//
	pout << "network stats\n";
	for (::NetFunc::Net::NameToVal::const_iterator kk = stats.begin(); kk != stats.end(); ++kk)
		pout << "stats_" << kk->first << "\t" << kk->second << "\t" << expected[kk->first] / (double) aux.NREP << "\t" << (pvals[kk->first] + 1.0) / (double) (aux.NREP + 1.0) << "\n";

	return true;
}
